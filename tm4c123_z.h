#ifndef __TM4C123_Z_H__
#define __TM4C123_Z_H__

#include "stdint.h"
#define GPIOA_APB GPIO_PORTA_DATA_BITS_R
#define GPIOA_AHB GPIO_PORTA_AHB_DATA_BITS_R
#define GPIOB_APB GPIO_PORTB_DATA_BITS_R
#define GPIOB_AHB GPIO_PORTB_AHB_DATA_BITS_R
#define GPIOC_APB GPIO_PORTC_DATA_BITS_R
#define GPIOC_AHB GPIO_PORTC_AHB_DATA_BITS_R
#define GPIOD_APB GPIO_PORTD_DATA_BITS_R
#define GPIOD_AHB GPIO_PORTD_AHB_DATA_BITS_R
#define GPIOE_APB GPIO_PORTE_DATA_BITS_R
#define GPIOE_AHB GPIO_PORTE_AHB_DATA_BITS_R
#define GPIOF_APB GPIO_PORTF_DATA_BITS_R
#define GPIOF_AHB GPIO_PORTF_AHB_DATA_BITS_R


void set_GPIO_DOUT(volatile unsigned long* GPIOx_AxB, uint8_t data);
uint8_t get_GPIO_DIN(volatile unsigned long* GPIOx_AxB, uint8_t msk);
void delay_ms(unsigned int delay);
void NVIC_ST_init(unsigned int cycles);

#endif // __TM4C123_Z_H__