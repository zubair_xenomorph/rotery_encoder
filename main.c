
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
//#include "lm4f120h5qr.h"
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "UART.h"
#include "bt_serial.h"
#include "tm4c123_z.h"

#define SWVAL (*((volatile unsigned long *)0x40025060))

uint8_t state =0;
uint8_t state1 =0;
uint8_t state2 =0;
uint8_t state3 =0;
int prev=0;
uint8_t prev_bt2=0;
unsigned int conf1=0;
unsigned int conf2=0;
uint8_t current_state;
static unsigned int time =0;
void DisableInterrupts(void); 
void EnableInterrupts(void);  
long StartCritical (void);    
void EndCritical(long sr);    
void WaitForInterrupt(void);  
bool toggle = true;

volatile int FallingEdge = 0;

void OutCRLF(void){
  UART_OutChar(CR);
  UART_OutChar(LF);
}

void EdgeCounter_Init(void){                          
  SYSCTL_RCGCGPIO_R |= 0x00000020; // (a) activate clock for port F
  FallingEdge = 0;             // (b) initialize counter
  GPIO_PORTF_DIR_R &= ~0x18;    // (c) make PF4 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~0x18;  //     disable alt funct on PF4
  GPIO_PORTF_DEN_R |= 0x18;     //     enable digital I/O on PF4   
  GPIO_PORTF_PCTL_R &= ~0x000FF000; // configure PF4 as GPIO
  GPIO_PORTF_AMSEL_R = 0;       //     disable analog functionality on PF
  GPIO_PORTF_PUR_R |= 0x18;     //     enable weak pull-up on PF4
  GPIO_PORTF_IS_R &= ~0x10;     // (d) PF4 is edge-sensitive
  GPIO_PORTF_IBE_R = 0x10;    //     PF4 is not both edges
  GPIO_PORTF_ICR_R = 0x10;      // (e) clear flag4
  GPIO_PORTF_IM_R |= 0x10;      // (f) arm interrupt on PF4 *** No IME bit as mentioned in Book ***
  NVIC_PRI7_R = (NVIC_PRI7_R&0xFF00FFFF)|0x00A00000; // (g) priority 5
  NVIC_EN0_R = 0x40000000;      // (h) enable interrupt 30 in NVIC
  
}


void GPIOF_Handler(void){
  GPIO_PORTF_ICR_R = 0x10;      
  
  state3=state2;
  state2=state1;
  state1=state;
  state=current_state;
  current_state = (SWVAL);
  if((current_state==0x18)&&(state==0x00)) FallingEdge++;
  else if((current_state==0x10)&&(state==0x08)) FallingEdge--;
  if(prev!=FallingEdge){
 BT_OutUDec((int)FallingEdge);
 BT_OutChar(' ');
 BT_OutChar(' ');
 BT_OutChar('\n');
  }
  prev=FallingEdge;
 
  
}


int main(void){
  SYSCTL_RCGCGPIO_R = 0x10;
  GPIO_PORTE_DIR_R |= 0xFF;
  GPIO_PORTE_DEN_R |= 0xFF;
  char c;
  PLL_Init();
  UART_Init();
  BT_Init();
  EdgeCounter_Init(); 
  
  
 
  
   NVIC_ST_CTRL_R=0;
  NVIC_ST_RELOAD_R=0x800000;
  NVIC_ST_CURRENT_R=0;
  NVIC_ST_CTRL_R=0x00000005;
  unsigned int time=0;
  while(1){
    c=BT_InChar();
    UART_OutChar(c);
    
    if(toggle == true)
    {
      if(abs(time-(unsigned int)NVIC_ST_CURRENT_R)>100000)
      {
      GPIO_PORTE_DATA_R^=0xFF;
      time=(unsigned int)NVIC_ST_CURRENT_R;
      }
      
    }

    
    
    if(c=='1')
    {
      GPIO_PORTE_DATA_R=0xFF;
      toggle = false;
    }
    else if(c=='0')
      {
      GPIO_PORTE_DATA_R=0x00;
      toggle = false;
    }
    else if(c=='b')
      toggle=true;
    
   

  }
}

